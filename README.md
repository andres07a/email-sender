# Email-sender

Simple aplicación para enviar correos en texto plano.

Ejemplo de uso:

    email-sender -user=miUsuario@ejemplo.net -password=passw0rd -to=emisor@correo.com -cc="cc@correo.com;cc2@ejemplo.net" -subject='titulo del correo' -body='Esto va en el cuerpo del correo :)'

### Opciones
- `-server=` asigna el servidor. 
- `-port=` asigna el puerto.