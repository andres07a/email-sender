package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"os"
	"strings"

	"gopkg.in/gomail.v2"
)

func main() {

	m := gomail.NewMessage()

	emailUserName := flag.String("user", "", "Define USUARIO para conectarse al servidor de correo.")
	emailUserPass := flag.String("password", "", "Define CONTRASEÑA para conectarse al servidor de correo.")

	emailFrom := flag.String("to", "", "Define EMISOR del correo.")
	emailCc := flag.String("cc", "", "Define DESTINATARIO(s) separado por ';'. EJ: \"correo1@ejemplo.com;correo2@ejemplo.com\".")
	emailSubject := flag.String("subject", "", "Define TÍTULO del correo.")
	emailBody := flag.String("body", "Cuerpo del correo vacío", "Define CUERPO del correo.")

	emailServer := flag.String("server", "correo.ejemplo.net", "Define SERVIDOR de correo.")
	emailPort := flag.Int("port", 587, "Define PUERTO del servidor de correo.")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, `Esta app envia correos en base al servidor correo.ejemplo.net:587
Ejemplos:
email-sender -user=miUsuario@ejemplo.net 
	-password=passw0rd 
	-to=emisor@correo.com
	-cc="cc@correo.com;cc2@ejemplo.net"
	-subject='titulo del correo'
	-body='Esto va en el cuerpo del correo :)'

Opciones:
`)
		flag.PrintDefaults()
	}

	flag.Parse()

	listTOemail := []string{}
	for _, dest := range strings.Split(*emailFrom, ";") {
		listTOemail = append(listTOemail, m.FormatAddress(dest, ""))
	}
	listTOemail = removeWhiteStrings(listTOemail)

	// asignar correos TO
	if len(listTOemail) > 0 {
		m.SetHeaders(map[string][]string{
			"To": listTOemail,
		})
	}

	listCCemail := []string{}
	for _, dest := range strings.Split(*emailCc, ";") {
		listCCemail = append(listCCemail, m.FormatAddress(dest, ""))
	}
	listCCemail = removeWhiteStrings(listCCemail)

	// asignar correos CC
	if len(listCCemail) > 0 {
		m.SetHeaders(map[string][]string{
			"Cc": listCCemail,
		})
	}

	// asignar cuerpo del correo
	m.SetHeaders(map[string][]string{
		"From":    []string{m.FormatAddress(*emailUserName, "")},
		"Bcc":     []string{*emailUserName},
		"Subject": []string{*emailSubject},
	})

	m.SetBody("text/plain", *emailBody)

	d := gomail.NewDialer(*emailServer, *emailPort, *emailUserName, *emailUserPass)

	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	if err := d.DialAndSend(m); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}

	fmt.Println("Correo enviado!")
}

func removeWhiteStrings(s []string) []string {
	salida := []string{}
	for _, v := range s {
		if v != "" {
			salida = append(salida, v)
		}
	}
	return salida
}
